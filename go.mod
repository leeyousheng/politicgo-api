module bitbucket.org/leeyousheng/politicgo-api

go 1.14

require (
	cloud.google.com/go/firestore v1.3.0
	firebase.google.com/go v3.13.0+incompatible
	github.com/google/martian v2.1.0+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/stretchr/testify v1.4.0
	github.com/urfave/negroni v1.0.0
	google.golang.org/api v0.31.0
	google.golang.org/grpc v1.31.1
)
