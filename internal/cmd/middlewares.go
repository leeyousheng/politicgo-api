package cmd

import (
	firebase "bitbucket.org/leeyousheng/politicgo-api/pkg/firebase"
	"bitbucket.org/leeyousheng/politicgo-api/pkg/webapp/server"
	goFirebase "firebase.google.com/go"
)

func middlewares(fApp *goFirebase.App) []server.Middleware {
	mws := []server.Middleware{
		// Insert middleware here
		firebase.NewAuth(fApp),
	}
	return mws
}
