package cmd

import (
	"context"
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/leeyousheng/politicgo-api/pkg/webapp/server"
	firebase "firebase.google.com/go"
)

type configurations struct {
	isCors bool
	port   string
}

// Config contains all the configurations that will be used by the application.
type Config struct {
	router       http.Handler
	firebase     *firebase.App
	configParams configurations
}

// InitApp initialise the config object according to the provided configurations.
func InitApp() *Config {
	firebaseApp := initFirebase()
	firestore, err := firebaseApp.Firestore(context.Background())
	if err != nil {
		panic(err)
	}

	configParams := configurations{isCors: false, port: "8080"}
	return &Config{
		router:       server.NewRouter("/api", exposeRoutes(firestore), middlewares(firebaseApp), configParams.isCors),
		firebase:     firebaseApp,
		configParams: configParams,
	}
}

// Terminate safely release the connections.
func (c *Config) Terminate() {
	// Do nothing
}

// Serve the server on the selected port.
func (c *Config) Serve() error {
	log.Printf("Starting Politicgo API Server service on port %s.", c.configParams.port)
	return http.ListenAndServe(fmt.Sprintf(":%s", c.configParams.port), c.router)
}

func initFirebase() *firebase.App {
	if app, err := firebase.NewApp(context.Background(), nil); err != nil {
		panic(err)
	} else {
		return app
	}
}
