package cmd

import (
	"bitbucket.org/leeyousheng/politicgo-api/internal/pkg/feed"
	"bitbucket.org/leeyousheng/politicgo-api/internal/pkg/post"
	"bitbucket.org/leeyousheng/politicgo-api/internal/pkg/profile"
	"bitbucket.org/leeyousheng/politicgo-api/internal/pkg/topic"
	"bitbucket.org/leeyousheng/politicgo-api/pkg/webapp/server"
	firestore "cloud.google.com/go/firestore"
)

func exposeRoutes(fc *firestore.Client) []server.Route {
	routes := []server.Route{}
	routes = append(routes, exposePostRoutes(fc)...)
	return routes
}

func exposePostRoutes(fc *firestore.Client) []server.Route {
	profile := profile.NewConfig(fc)
	post := post.NewConfig(fc, profile.Manager)
	feed := feed.NewConfig(fc, profile.Manager)
	topic := topic.NewConfig(fc)

	return []server.Route{
		post.API.GetPost(),
		post.API.GetPosts(),
		post.API.CreatePost(),
		post.API.DeletePost(),
		post.API.ReplacePost(),
		post.API.UpdatePost(),
		topic.API.GetTopic(),
		topic.API.GetTopics(),
		topic.API.CreateTopic(),
		topic.API.DeleteTopic(),
		topic.API.ReplaceTopic(),
		topic.API.UpdateTopic(),
		profile.API.GetProfile(),
		profile.API.CreateProfile(),
		feed.API.GetFeed(),
	}
}
