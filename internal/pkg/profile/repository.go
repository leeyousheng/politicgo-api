package profile

import (
	"context"

	"bitbucket.org/leeyousheng/politicgo-api/pkg/firebase"
	"bitbucket.org/leeyousheng/politicgo-api/pkg/webapp/server"
	"cloud.google.com/go/firestore"
	"google.golang.org/api/iterator"
)

type repository struct {
	firebase.Repository
}

func newRepository(collectionRef *firestore.CollectionRef) repository {
	return repository{
		firebase.NewFirebaseRepository(collectionRef),
	}
}

func (r *repository) findOneByAuthID(res *Profile, authID string) error {
	doc, err := r.Query().Where("AuthID", "==", authID).Documents(context.Background()).Next()
	if err != nil {
		if err == iterator.Done {
			return server.NewNotFoundError("No profile found.")
		}
		return err
	}
	doc.DataTo(res)
	return nil
}

func (r *repository) checkExist(authID string) (bool, error) {
	res, err := r.Query().Where("AuthID", "==", authID).Documents(context.Background()).GetAll()
	if err != nil {
		return false, err
	}
	return len(res) > 0, nil
}
