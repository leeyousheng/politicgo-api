package profile

import "bitbucket.org/leeyousheng/politicgo-api/pkg/webapp/entity"

// Profile stores the app data of the user
type Profile struct {
	entity.AuditableStruct
	entity.DistinctStruct
	Topics      []string `json:"topics"`
	AuthID      string   `json:"-"`
	DisplayName string   `json:"displayName"`
	PhotoURL    string   `json:"photoURL"`
}

// NewProfile initializes necessary embedded structs
func NewProfile() Profile {
	return Profile{
		AuditableStruct: entity.AuditableStruct{},
		DistinctStruct:  entity.DistinctStruct{},
	}
}

// PreparePut filters out fields that should not be updated
func preparePut(ref Profile, input Profile) Profile {
	input.ID = ref.ID
	input.CreatedOn = ref.CreatedOn
	input.CreatedBy = ref.CreatedBy
	return input
}

// PreparePatch replaces values of only selected fields
func preparePatch(ref Profile, input map[string]interface{}) Profile {
	// TODO Implement selection of fields to be updated.
	// for k, v := range input {
	// 	switch k {
	// 	case "text":
	// 		ref.Text = v.(string)
	// 	}
	// }
	return ref
}
