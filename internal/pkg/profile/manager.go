package profile

import (
	"fmt"

	"bitbucket.org/leeyousheng/politicgo-api/pkg/webapp/entity"
	"bitbucket.org/leeyousheng/politicgo-api/pkg/webapp/server"
	"cloud.google.com/go/firestore"
)

// Manager manages the functions used when talking to the profile module
type Manager struct {
	r *repository
}

func newManager(collectionRef *firestore.CollectionRef) *Manager {
	r := newRepository(collectionRef)
	return &Manager{r: &r}
}

// GetProfileByFbAuthID returns the profile of the user provided
func (m *Manager) GetProfileByFbAuthID(authID string) (Profile, error) {
	res := Profile{}
	err := m.r.findOneByAuthID(&res, authID)

	if err != nil {
		return Profile{}, err
	}
	return res, nil
}

func (m *Manager) createProfile(p Profile, authID string) (Profile, error) {
	p.AuthID = authID
	err := m.r.CreateWithGenID(&p, authID)
	if err != nil {
		return Profile{}, err
	}
	return p, nil
}

func (m *Manager) deleteProfile(id string) (server.Result, error) {
	err := m.r.Delete(id)
	if err != nil {
		return server.Result{}, err
	}
	return server.NewResult(fmt.Sprintf("profile with id: %s deleted", id)), nil
}

func (m *Manager) replaceProfile(id string, input Profile, user string) (Profile, error) {
	err := m.r.FullUpdate(id, &input, user)
	if err != nil {
		return Profile{}, err
	}

	return input, nil
}

func (m *Manager) patchProfile(id string, input entity.Input, user string) (Profile, error) {
	res := Profile{}
	if err := m.r.Update(id, input, user, &res); err != nil {
		return Profile{}, err
	}
	return res, nil
}

func (m *Manager) checkProfileExistByFbAuthID(authID string) (bool, error) {
	res, err := m.r.checkExist(authID)
	if err != nil {
		return false, err
	}
	return res, nil
}
