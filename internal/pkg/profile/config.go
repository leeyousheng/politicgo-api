package profile

import (
	"cloud.google.com/go/firestore"
)

// Config exposes the various services in the Module
type Config struct {
	Manager *Manager
	API     *API
}

// NewConfig handles the creation of the services
func NewConfig(db *firestore.Client) *Config {
	manager := newManager(db.Collection("profiles"))
	api := NewAPI("profiles", manager)

	return &Config{
		Manager: manager,
		API:     api,
	}
}
