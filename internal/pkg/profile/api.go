package profile

import (
	"encoding/json"
	"fmt"
	"net/http"

	"bitbucket.org/leeyousheng/politicgo-api/pkg/webapp/server"
)

// API stores all the api present for posts
type API struct {
	rootURL string
	manager *Manager
}

// NewAPI instantiates a new API instance
func NewAPI(rootURL string, manager *Manager) *API {
	return &API{rootURL: rootURL, manager: manager}
}

// CreateProfile creates the profile requested.
func (api *API) CreateProfile() server.Route {
	return server.NewPostRoute(
		"CreateProfile",
		fmt.Sprintf("/%s", api.rootURL),
		func(r *http.Request) (interface{}, error) {
			entity := Profile{}
			err := json.NewDecoder(r.Body).Decode(&entity)
			if err != nil {
				return nil, server.NewBadRequestError(err.Error())
			}

			authID := r.Header.Get("uid")
			exist, err := api.manager.checkProfileExistByFbAuthID(authID)
			if err != nil {
				return nil, err
			}
			if exist {
				return nil, server.NewBadRequestError("Existing user, cannot create new profile.")
			}
			return api.manager.createProfile(entity, authID)
		},
	)
}

// GetProfile returns the Profile requested.
func (api *API) GetProfile() server.Route {
	return server.NewGetRoute(
		"GetProfile",
		fmt.Sprintf("/%s", api.rootURL),
		func(r *http.Request) (interface{}, error) {
			return api.manager.GetProfileByFbAuthID(r.Header.Get("uid"))
		},
	)
}

// TODO implement ReplaceProfile, UpdateProfile, DeleteProfile
// // ReplaceProfile replaces the entire Profile requested.
// func (api *API) ReplaceProfile() server.Route {
// 	return server.NewPutRoute(
// 		"ReplaceProfile",
// 		fmt.Sprintf("/%s/{id}", api.rootURL),
// 		func(r *http.Request) (interface{}, error) {
// 			entity := Profile{}
// 			err := json.NewDecoder(r.Body).Decode(&entity)
// 			if err != nil {
// 				return nil, server.NewBadRequestError(err.Error())
// 			}
// 			return api.manager.replaceProfile(mux.Vars(r)["id"], entity, r.Header.Get("uid"))
// 		},
// 	)
// }

// // UpdateProfile updates the profile requested.
// func (api *API) UpdateProfile() server.Route {
// 	return server.NewPatchRoute(
// 		"UpdateProfile",
// 		fmt.Sprintf("/%s/{id}", api.rootURL),
// 		func(r *http.Request) (interface{}, error) {
// 			entity := map[string]interface{}{}
// 			err := json.NewDecoder(r.Body).Decode(&entity)
// 			if err != nil {
// 				return nil, server.NewBadRequestError(err.Error())
// 			}
// 			return api.manager.patchProfile(mux.Vars(r)["id"], entity, r.Header.Get("uid"))
// 		},
// 	)
// }

// // DeleteProfile deletes the profile requested
// func (api *API) DeleteProfile() server.Route {
// 	return server.NewDeleteRoute(
// 		"DeleteProfile",
// 		fmt.Sprintf("/%s/{id}", api.rootURL),
// 		func(r *http.Request) (interface{}, error) {
// 			id := mux.Vars(r)["id"]
// 			return api.manager.deleteProfile(id)
// 		},
// 	)
// }
