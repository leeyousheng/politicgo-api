package post

import (
	"testing"

	"bitbucket.org/leeyousheng/politicgo-api/internal/pkg/profile"
	"bitbucket.org/leeyousheng/politicgo-api/pkg/webapp/entity"
	"github.com/stretchr/testify/assert"
)

func TestPreparePostForCreation_shouldInitTopicsIfNil(t *testing.T) {
	newPost := preparePostForCreation(Post{Topics: nil}, profile.Profile{})
	assert.Equal(t, []string{""}, newPost.Topics)
}

func TestPreparePostForCreation_shouldRetainTopicsIfNotNil(t *testing.T) {
	newPost := preparePostForCreation(Post{Topics: []string{"topic1"}}, profile.Profile{})
	assert.Equal(t, []string{"", "topic1"}, newPost.Topics)
}

func TestPreparePostForCreation_shouldPrependProfileIDToTopics(t *testing.T) {
	newPost := preparePostForCreation(Post{Topics: []string{"topic1"}}, profile.Profile{DistinctStruct: entity.DistinctStruct{ID: "TestID"}})
	assert.Equal(t, []string{"TestID", "topic1"}, newPost.Topics)
}
