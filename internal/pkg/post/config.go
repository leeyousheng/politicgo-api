package post

import (
	"bitbucket.org/leeyousheng/politicgo-api/internal/pkg/profile"
	"cloud.google.com/go/firestore"
)

// Config exposes the various services in the Module
type Config struct {
	manager *manager
	API     *API
}

// NewConfig handles the creation of the services
func NewConfig(db *firestore.Client, pm *profile.Manager) *Config {
	manager := newManager(db.Collection("posts"), pm)
	api := NewAPI("posts", manager)

	return &Config{
		manager: manager,
		API:     api,
	}
}
