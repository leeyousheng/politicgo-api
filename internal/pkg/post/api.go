package post

import (
	"encoding/json"
	"fmt"
	"net/http"

	"bitbucket.org/leeyousheng/politicgo-api/pkg/webapp/config"
	"bitbucket.org/leeyousheng/politicgo-api/pkg/webapp/server"
	"github.com/gorilla/mux"
)

// API stores all the api present for posts
type API struct {
	rootURL string
	manager *manager
}

// NewAPI instantiates a new API instance
func NewAPI(rootURL string, manager *manager) *API {
	return &API{rootURL: rootURL, manager: manager}
}

// GetPosts returns a collection of posts.
func (api *API) GetPosts() server.Route {
	return server.NewGetRoute(
		"GetPosts",
		fmt.Sprintf("/%s", api.rootURL),
		func(r *http.Request) (interface{}, error) {
			return api.manager.getPostsPaged(config.NewPageableByMap(r.URL.Query()))
		},
	)
}

// CreatePost creates the post requested.
func (api *API) CreatePost() server.Route {
	return server.NewPostRoute(
		"CreatePost",
		fmt.Sprintf("/%s", api.rootURL),
		func(r *http.Request) (interface{}, error) {
			entity := Post{}
			err := json.NewDecoder(r.Body).Decode(&entity)
			if err != nil {
				return nil, server.NewBadRequestError(err.Error())
			}
			return api.manager.createPost(entity, r.Header.Get("uid"))
		},
	)
}

// GetPost returns the post requested.
func (api *API) GetPost() server.Route {
	return server.NewGetRoute(
		"GetPost",
		fmt.Sprintf("/%s/{id}", api.rootURL),
		func(r *http.Request) (interface{}, error) {
			id := mux.Vars(r)["id"]
			return api.manager.getPost(id)
		},
	)
}

// ReplacePost replaces the entire post requested.
func (api *API) ReplacePost() server.Route {
	return server.NewPutRoute(
		"ReplacePost",
		fmt.Sprintf("/%s/{id}", api.rootURL),
		func(r *http.Request) (interface{}, error) {
			entity := Post{}
			err := json.NewDecoder(r.Body).Decode(&entity)
			if err != nil {
				return nil, server.NewBadRequestError(err.Error())
			}
			return api.manager.replacePost(mux.Vars(r)["id"], entity, r.Header.Get("uid"))
		},
	)
}

// UpdatePost updates the post requested.
func (api *API) UpdatePost() server.Route {
	return server.NewPatchRoute(
		"UpdatePost",
		fmt.Sprintf("/%s/{id}", api.rootURL),
		func(r *http.Request) (interface{}, error) {
			entity := map[string]interface{}{}
			err := json.NewDecoder(r.Body).Decode(&entity)
			if err != nil {
				return nil, server.NewBadRequestError(err.Error())
			}
			return api.manager.patchPost(mux.Vars(r)["id"], entity, r.Header.Get("uid"))
		},
	)
}

// DeletePost deletes the post requested
func (api *API) DeletePost() server.Route {
	return server.NewDeleteRoute(
		"DeletePost",
		fmt.Sprintf("/%s/{id}", api.rootURL),
		func(r *http.Request) (interface{}, error) {
			id := mux.Vars(r)["id"]
			return api.manager.deletePost(id)
		},
	)
}
