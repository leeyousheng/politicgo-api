package post

import (
	"fmt"

	"bitbucket.org/leeyousheng/politicgo-api/internal/pkg/profile"
	"bitbucket.org/leeyousheng/politicgo-api/pkg/firebase"
	"bitbucket.org/leeyousheng/politicgo-api/pkg/webapp/config"
	"bitbucket.org/leeyousheng/politicgo-api/pkg/webapp/entity"
	"bitbucket.org/leeyousheng/politicgo-api/pkg/webapp/server"
	"cloud.google.com/go/firestore"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type manager struct {
	r  *firebase.Repository
	pm *profile.Manager
}

func newManager(collectionRef *firestore.CollectionRef, profileManager *profile.Manager) *manager {
	r := firebase.NewFirebaseRepository(collectionRef)
	return &manager{r: &r, pm: profileManager}
}

func (m *manager) getPosts() ([]Post, error) {
	res := []Post{}
	err := m.r.Find(&res)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (m *manager) getPostsPaged(pageable config.Pageable) (entity.Page, error) {
	res := []Post{}
	err := m.r.FindPaged(&res, &pageable, []firebase.CriteriaConfig{})
	if err != nil {
		return entity.Page{}, err
	}
	return entity.NewPage(res, len(res), pageable.Size()), nil
}

func (m *manager) getPost(id string) (Post, error) {
	res := Post{}
	err := m.r.FindOne(id, &res)
	if err != nil {
		if status.Code(err) == codes.NotFound {
			return Post{}, server.NewNotFoundError(fmt.Sprintf("post with id: %s not found", id))
		}
		return Post{}, err
	}
	return res, nil
}

func (m *manager) createPost(p Post, user string) (Post, error) {
	profile, err := m.pm.GetProfileByFbAuthID(user)
	if err != nil {
		return Post{}, err
	}

	p = preparePostForCreation(p, profile)

	err = m.r.CreateWithGenID(&p, user)
	if err != nil {
		return Post{}, err
	}
	return p, nil
}

func preparePostForCreation(post Post, profile profile.Profile) Post {
	post.Author = NewAuthorFromProfile(profile)
	if post.Topics == nil {
		post.Topics = []string{}
	}
	post.Topics = append([]string{profile.ID}, post.Topics...)

	return post
}

func (m *manager) deletePost(id string) (server.Result, error) {
	err := m.r.Delete(id)
	if err != nil {
		return server.Result{}, err
	}
	return server.NewResult(fmt.Sprintf("post with id: %s deleted", id)), nil
}

func (m *manager) replacePost(id string, input Post, user string) (Post, error) {
	err := m.r.FullUpdate(id, &input, user)
	if err != nil {
		return Post{}, err
	}

	return input, nil
}

func (m *manager) patchPost(id string, input entity.Input, user string) (Post, error) {
	res := Post{}
	if err := m.r.Update(id, input, user, &res); err != nil {
		return Post{}, err
	}
	return res, nil
}
