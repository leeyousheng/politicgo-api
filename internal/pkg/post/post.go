package post

import (
	"bitbucket.org/leeyousheng/politicgo-api/internal/pkg/profile"
	"bitbucket.org/leeyousheng/politicgo-api/pkg/webapp/entity"
)

// Author represents the basic information of the creator
type Author struct {
	ID       string `json:"-"`
	Name     string `json:"name"`
	PhotoURL string `json:"photoURL"`
}

// NewAuthorFromProfile creates an author using profile information
func NewAuthorFromProfile(p profile.Profile) Author {
	return Author{
		ID:       p.ID,
		Name:     p.DisplayName,
		PhotoURL: p.PhotoURL,
	}
}

// Post stores the attributes that constitutes a short article
type Post struct {
	entity.AuditableStruct
	entity.DistinctStruct
	Author Author   `json:"author"`
	Text   string   `json:"text"`
	Topics []string `json:"topics"`
}

// PreparePut filters out fields that should not be updated
func preparePut(ref Post, input Post) Post {
	input.ID = ref.ID
	input.CreatedOn = ref.CreatedOn
	input.CreatedBy = ref.CreatedBy
	return input
}

// PreparePatch replaces values of only selected fields
func preparePatch(ref Post, input map[string]interface{}) Post {
	for k, v := range input {
		switch k {
		case "text":
			ref.Text = v.(string)
		}
	}
	return ref
}
