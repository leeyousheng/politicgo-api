package topic

import (
	"cloud.google.com/go/firestore"
)

// Config exposes the various services in the Module
type Config struct {
	manager *manager
	API     *API
}

// NewConfig handles the creation of the services
func NewConfig(db *firestore.Client) *Config {
	manager := newManager(db.Collection("topics"))
	api := NewAPI("topics", manager)

	return &Config{
		manager: manager,
		API:     api,
	}
}
