package topic

import (
	"strings"

	"bitbucket.org/leeyousheng/politicgo-api/pkg/webapp/entity"
	"bitbucket.org/leeyousheng/politicgo-api/pkg/webapp/server"
)

// Topic stores the attributes that constitutes a categorization of posts
type Topic struct {
	entity.AuditableStruct
	entity.DistinctStruct
	Code        string `json:"code"`
	DisplayName string `json:"displayName"`
	Description string `json:"description"`
}

func (t *Topic) validate() error {
	// code must be lower case and no space.
	f := strings.Fields(t.Code)
	if len(f) != 1 {
		return server.NewBadRequestError("code should not contain whitespaces")
	}
	if strings.ToLower(f[0]) != f[0] {
		return server.NewBadRequestError("code should be all lower case")
	}

	t.Code = f[0]
	return nil
}

// PreparePut filters out fields that should not be updated
// TODO can be extracted out to be handled as a common function
func preparePut(ref Topic, input Topic) Topic {
	input.ID = ref.ID
	input.CreatedOn = ref.CreatedOn
	input.CreatedBy = ref.CreatedBy
	return input
}

// PreparePatch replaces values of only selected fields
func preparePatch(ref Topic, input map[string]interface{}) Topic {
	for k, v := range input {
		switch k {
		case "description":
			ref.Description = v.(string)
		}
	}
	return ref
}
