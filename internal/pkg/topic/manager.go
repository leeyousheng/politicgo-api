package topic

import (
	"fmt"

	"bitbucket.org/leeyousheng/politicgo-api/pkg/firebase"
	"bitbucket.org/leeyousheng/politicgo-api/pkg/webapp/config"
	"bitbucket.org/leeyousheng/politicgo-api/pkg/webapp/entity"
	"bitbucket.org/leeyousheng/politicgo-api/pkg/webapp/server"
	"cloud.google.com/go/firestore"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type manager struct {
	r *firebase.Repository
}

func newManager(collectionRef *firestore.CollectionRef) *manager {
	r := firebase.NewFirebaseRepository(collectionRef)
	return &manager{r: &r}
}

func (m *manager) getTopics() ([]Topic, error) {
	res := []Topic{}
	err := m.r.Find(&res)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (m *manager) getTopicsPaged(pageable config.Pageable) (entity.Page, error) {
	res := []Topic{}
	err := m.r.FindPaged(&res, &pageable, []firebase.CriteriaConfig{})
	if err != nil {
		return entity.Page{}, err
	}
	return entity.NewPage(res, len(res), pageable.Size()), nil
}

func (m *manager) getTopic(id string) (Topic, error) {
	res := Topic{}
	err := m.r.FindOne(id, &res)
	if err != nil {
		if status.Code(err) == codes.NotFound {
			return Topic{}, server.NewNotFoundError(fmt.Sprintf("topic with id: %s not found", id))
		}
		return Topic{}, err
	}
	return res, nil
}

func (m *manager) createTopic(topic Topic, user string) (Topic, error) {
	topic.validate()

	if topic.DisplayName == "" {
		topic.DisplayName = topic.Code
	}

	err := m.r.FindOne(topic.Code, &Topic{})

	if err == nil {
		return Topic{}, server.NewBadRequestError("Topic already exists.")
	}

	if status.Code(err) != codes.NotFound {
		return Topic{}, err
	}

	err = m.r.Create(topic.Code, &topic, user)
	return topic, err
}

func (m *manager) deleteTopic(code string) (server.Result, error) {
	err := m.r.Delete(code)
	if err != nil {
		return server.Result{}, err
	}
	return server.NewResult(fmt.Sprintf("topic with code: %s deleted", code)), nil
}

func (m *manager) replaceTopic(code string, input Topic, user string) (Topic, error) {
	err := m.r.FullUpdate(code, &input, user)
	if err != nil {
		return Topic{}, err
	}

	return input, nil
}

func (m *manager) patchTopic(code string, input entity.Input, user string) (Topic, error) {
	res := Topic{}
	if err := m.r.Update(code, input, user, &res); err != nil {
		return Topic{}, err
	}
	return res, nil
}
