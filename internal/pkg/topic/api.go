package topic

import (
	"encoding/json"
	"fmt"
	"net/http"

	"bitbucket.org/leeyousheng/politicgo-api/pkg/webapp/config"
	"bitbucket.org/leeyousheng/politicgo-api/pkg/webapp/server"
	"github.com/gorilla/mux"
)

// API stores all the api present for Topics
type API struct {
	rootURL string
	manager *manager
}

// NewAPI instantiates a new API instance
func NewAPI(rootURL string, manager *manager) *API {
	return &API{rootURL: rootURL, manager: manager}
}

// GetTopics returns a collection of topics.
func (api *API) GetTopics() server.Route {
	return server.NewGetRoute(
		"GetTopics",
		fmt.Sprintf("/%s", api.rootURL),
		func(r *http.Request) (interface{}, error) {
			return api.manager.getTopicsPaged(config.NewPageableByMap(r.URL.Query()))
		},
	)
}

// CreateTopic creates the topics requested.
func (api *API) CreateTopic() server.Route {
	return server.NewPostRoute(
		"CreateTopic",
		fmt.Sprintf("/%s", api.rootURL),
		func(r *http.Request) (interface{}, error) {
			entity := Topic{}
			err := json.NewDecoder(r.Body).Decode(&entity)
			if err != nil {
				return nil, server.NewBadRequestError(err.Error())
			}
			return api.manager.createTopic(entity, r.Header.Get("uid"))
		},
	)
}

// GetTopic returns the topic requested.
func (api *API) GetTopic() server.Route {
	return server.NewGetRoute(
		"GetTopic",
		fmt.Sprintf("/%s/{id}", api.rootURL),
		func(r *http.Request) (interface{}, error) {
			id := mux.Vars(r)["id"]
			return api.manager.getTopic(id)
		},
	)
}

// ReplaceTopic replaces the entire topic requested.
func (api *API) ReplaceTopic() server.Route {
	return server.NewPutRoute(
		"ReplaceTopic",
		fmt.Sprintf("/%s/{id}", api.rootURL),
		func(r *http.Request) (interface{}, error) {
			entity := Topic{}
			err := json.NewDecoder(r.Body).Decode(&entity)
			if err != nil {
				return nil, server.NewBadRequestError(err.Error())
			}
			return api.manager.replaceTopic(mux.Vars(r)["id"], entity, r.Header.Get("uid"))
		},
	)
}

// UpdateTopic updates the topic requested.
func (api *API) UpdateTopic() server.Route {
	return server.NewPatchRoute(
		"UpdateTopic",
		fmt.Sprintf("/%s/{id}", api.rootURL),
		func(r *http.Request) (interface{}, error) {
			entity := map[string]interface{}{}
			err := json.NewDecoder(r.Body).Decode(&entity)
			if err != nil {
				return nil, server.NewBadRequestError(err.Error())
			}
			return api.manager.patchTopic(mux.Vars(r)["id"], entity, r.Header.Get("uid"))
		},
	)
}

// DeleteTopic deletes the topic requested
func (api *API) DeleteTopic() server.Route {
	return server.NewDeleteRoute(
		"DeleteTopic",
		fmt.Sprintf("/%s/{id}", api.rootURL),
		func(r *http.Request) (interface{}, error) {
			id := mux.Vars(r)["id"]
			return api.manager.deleteTopic(id)
		},
	)
}
