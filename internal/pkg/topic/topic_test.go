package topic

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestValidate_shouldThrowError_givenCodeContainsWhitespaces(t *testing.T) {
	testWhiteSpaceFixture := func(t *testing.T, scenario string) {
		topic := Topic{Code: scenario}
		err := topic.validate()
		assert.Error(t, err)
		assert.Equal(t, "code should not contain whitespaces", err.Error())
	}

	testWhiteSpaceFixture(t, "test test")
}

func TestValidate_shouldThrowError_givenContainsCapitalLetters(t *testing.T) {
	testCapsFixture := func(t *testing.T, scenario string) {
		topic := Topic{Code: scenario}
		err := topic.validate()
		assert.Error(t, err)
		assert.Equal(t, "code should be all lower case", err.Error())
	}

	testCapsFixture(t, "testTest")
}

func TestValidate_shouldSucceed_giveValidInputs(t *testing.T) {
	testValidFixture := func(t *testing.T, scenario string) {
		topic := Topic{Code: scenario}
		err := topic.validate()
		assert.NoError(t, err)
		assert.Equal(t, strings.TrimSpace(scenario), topic.Code)
	}

	testValidFixture(t, "testtest")
	testValidFixture(t, "test_test")
	testValidFixture(t, "test2test")
	testValidFixture(t, "    testtest    ")
}
