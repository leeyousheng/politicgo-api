package feed

import (
	"bitbucket.org/leeyousheng/politicgo-api/internal/pkg/post"
	"bitbucket.org/leeyousheng/politicgo-api/internal/pkg/profile"
	"bitbucket.org/leeyousheng/politicgo-api/pkg/firebase"
	"bitbucket.org/leeyousheng/politicgo-api/pkg/webapp/config"
	"bitbucket.org/leeyousheng/politicgo-api/pkg/webapp/entity"
	"cloud.google.com/go/firestore"
)

type manager struct {
	r *firebase.Repository
}

func newManager(collectionRef *firestore.CollectionRef) *manager {
	r := firebase.NewFirebaseRepository(collectionRef)
	return &manager{r: &r}
}

func (m *manager) getFeed(p profile.Profile, pageable config.Pageable) (entity.Page, error) {
	res := []post.Post{}
	err := m.r.FindPaged(&res, &pageable, []firebase.CriteriaConfig{{Field: "Topics", Operator: "array-contains-any", Value: append(p.Topics, p.ID)}})

	if err != nil {
		return entity.Page{}, err
	}
	return entity.NewPage(res, len(res), pageable.Size()), nil
}
