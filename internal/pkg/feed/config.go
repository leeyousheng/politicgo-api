package feed

import (
	"bitbucket.org/leeyousheng/politicgo-api/internal/pkg/profile"
	"cloud.google.com/go/firestore"
)

// Config exposes the various services in the Module
type Config struct {
	manager        *manager
	API            *API
	profileManager *profile.Manager
}

// NewConfig handles the creation of the services
func NewConfig(db *firestore.Client, profileManager *profile.Manager) *Config {
	manager := newManager(db.Collection("posts"))
	api := NewAPI("feed", manager, profileManager)

	return &Config{
		manager:        manager,
		API:            api,
		profileManager: profileManager,
	}
}
