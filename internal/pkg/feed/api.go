package feed

import (
	"fmt"
	"net/http"

	"bitbucket.org/leeyousheng/politicgo-api/internal/pkg/profile"
	"bitbucket.org/leeyousheng/politicgo-api/pkg/webapp/config"
	"bitbucket.org/leeyousheng/politicgo-api/pkg/webapp/server"
)

// API stores all the api present for posts
type API struct {
	rootURL        string
	manager        *manager
	profileManager *profile.Manager
}

// NewAPI instantiates a new API instance
func NewAPI(rootURL string, manager *manager, profileManager *profile.Manager) *API {
	return &API{rootURL: rootURL, manager: manager, profileManager: profileManager}
}

// GetFeed returns a collection of posts linked to the feed.
func (api *API) GetFeed() server.Route {
	return server.NewGetRoute(
		"GetFeed",
		fmt.Sprintf("/%s", api.rootURL),
		func(r *http.Request) (interface{}, error) {
			profile, err := api.profileManager.GetProfileByFbAuthID(r.Header.Get("uid"))
			if err != nil {
				return nil, err
			}
			return api.manager.getFeed(profile, config.NewPageableByMap(r.URL.Query()))
		},
	)
}
