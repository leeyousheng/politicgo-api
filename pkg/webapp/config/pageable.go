package config

import (
	"strconv"

	"bitbucket.org/leeyousheng/politicgo-api/pkg/firebase"
)

// Pageable contains the configuration parameters to retrieve a page.
type Pageable struct {
	size    int
	cursor  string
	orderBy *OrderBy
}

// NewPageableByMap allows instantiation by map input
func NewPageableByMap(paramMap map[string][]string) Pageable {
	var orderKey, direction, size, cursor string

	if paramMap["orderKey"] != nil {
		orderKey = paramMap["orderKey"][0]
	}

	if paramMap["direction"] != nil {
		direction = paramMap["direction"][0]
	}

	if paramMap["size"] != nil {
		size = paramMap["size"][0]
	}

	if paramMap["cursor"] != nil {
		cursor = paramMap["cursor"][0]
	}

	return NewPageable(size, cursor, orderKey, direction)
}

// NewPageable creates pageable config with default values.
func NewPageable(size, cursor, orderKey, direction string) Pageable {

	s := 4
	oKey, oDir := "UpdatedOn", "desc"

	if newS, err := strconv.Atoi(size); err == nil && newS > 0 {
		s = newS
	}
	if s > 50 {
		s = 50
	}

	if orderKey != "" {
		oKey = orderKey
	}

	if direction != "" {
		oDir = direction
	}

	return Pageable{size: s, cursor: cursor, orderBy: NewOrderBy(oKey, oDir)}
}

// Size specifies the page size.
func (p *Pageable) Size() int {
	return p.size
}

// Cursor specifies the document reference for pagination
func (p *Pageable) Cursor() string {
	return p.cursor
}

// OrderBy specifies the key and direction of sorting
func (p *Pageable) OrderBy() firebase.OrderBy {
	return p.orderBy
}
