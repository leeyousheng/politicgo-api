package config

import (
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
)

var defaultSize = 4
var maxSize = 50
var defaultPage = 1

func TestNewPageable_shouldUseValue_givenValidSize(t *testing.T) {
	testValidSizeFixture := func(t *testing.T, scenario string) {
		res := NewPageable(scenario, "", "", "")
		s, _ := strconv.Atoi(scenario)
		assert.Equal(t, s, res.Size())
	}

	testValidSizeFixture(t, "1")
	testValidSizeFixture(t, "50")
}

func TestNewPageable_shouldUseValue_givenAboveMaxSize(t *testing.T) {
	testExceedMaxSizeFixure := func(t *testing.T, scenario string) {
		res := NewPageable(scenario, "", "", "")
		assert.Equal(t, maxSize, res.Size())
	}

	testExceedMaxSizeFixure(t, "51")
	testExceedMaxSizeFixure(t, "100")
}

func TestNewPageable_shouldUseDefaultValue_givenSizeEmptyOrInvalid(t *testing.T) {
	var testDefaultSizeFixture = func(t *testing.T, scenario string) {
		res := NewPageable(scenario, "", "", "")
		assert.Equal(t, defaultSize, res.Size())
	}

	testDefaultSizeFixture(t, "")
	testDefaultSizeFixture(t, "0")
	testDefaultSizeFixture(t, "-1")
	testDefaultSizeFixture(t, "error")
}
