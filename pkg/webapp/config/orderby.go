package config

// OrderBy contains the configuration neccessary for sorting
type OrderBy struct {
	key       string
	direction string
}

// NewOrderBy creates a new order by config
func NewOrderBy(key string, direction string) *OrderBy {
	return &OrderBy{key: key, direction: direction}
}

// Key returns the field of which to be ordered against
func (o *OrderBy) Key() string {
	return o.key
}

// Direction returns the direction of ordering
func (o *OrderBy) Direction() string {
	return o.direction
}
