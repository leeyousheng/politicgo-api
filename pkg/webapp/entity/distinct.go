package entity

// DistinctStruct entity signifies that the entity is unique
type DistinctStruct struct {
	ID string `json:"id"`
}

// OverwriteDistinctFields overwrites the ID field with the provided id
func (ds *DistinctStruct) OverwriteDistinctFields(id string) {
	ds.ID = id
}

// Distinct contains the methods related to a distinct entity
type Distinct interface {
	OverwriteDistinctFields(id string)
}
