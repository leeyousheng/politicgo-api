package entity

// Page contains page information and items
type Page struct {
	Items   interface{} `json:"items"`
	HasMore bool        `json:"hasMore"`
}

// NewPage creates a page entity with hasMore as true only if the items retrieved is sane as that is requested.
func NewPage(items interface{}, size int, requestSize int) Page {
	return Page{
		Items:   items,
		HasMore: size == requestSize,
	}
}
