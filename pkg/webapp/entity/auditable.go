package entity

import "time"

// AuditableStruct entity stores the audit meta data
type AuditableStruct struct {
	CreatedOn time.Time `json:"createdOn"`
	CreatedBy string    `json:"createdBy"`
	UpdatedOn time.Time `json:"updatedOn"`
	UpdatedBy string    `json:"updatedBy"`
}

// PopulateCreatedInfo populate created on time and created by name
func (a *AuditableStruct) PopulateCreatedInfo(user string) {
	if user == "" {
		user = "unknown"
	}
	time := time.Now()
	a.CreatedOn = time
	a.CreatedBy = user
	a.UpdatedOn = time
	a.UpdatedBy = user
}

// PopulateUpdatedInfo populate updated on time and updated by name
func (a *AuditableStruct) PopulateUpdatedInfo(user string) {
	if user == "" {
		user = "unknown"
	}
	a.UpdatedOn = time.Now()
	a.UpdatedBy = user
}

// OverwriteAuditFields ensures that created info is not changed
func (a *AuditableStruct) OverwriteAuditFields(ref Auditable) {
	a.CreatedBy = ref.getCreatedBy()
	a.CreatedOn = ref.getCreatedOn()
}

func (a *AuditableStruct) getCreatedOn() time.Time {
	return a.CreatedOn
}

func (a *AuditableStruct) getCreatedBy() string {
	return a.CreatedBy
}

// Auditable contains the methods to handle audit info
type Auditable interface {
	PopulateCreatedInfo(user string)
	PopulateUpdatedInfo(user string)
	getCreatedOn() time.Time
	getCreatedBy() string
	OverwriteAuditFields(ref Auditable)
}
