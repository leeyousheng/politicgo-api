package entity

// Input is a generic input that can be used during API calls
type Input = map[string]interface{}
