package server

import "net/http"

// Err is the custom err object with status code
type Err struct {
	msg  string
	Code int
}

// NewError creates a new error with status code
func NewError(msg string, code int) Err {
	return Err{msg: msg, Code: code}
}

// NewBadRequestError creates an error with status 400
func NewBadRequestError(msg string) Err {
	return NewError(msg, http.StatusBadRequest)
}

// NewNotFoundError creates an error with status 404
func NewNotFoundError(msg string) Err {
	return NewError(msg, http.StatusNotFound)
}

func (e Err) Error() string {
	return e.msg
}
