package server

import "net/http"

// Handler is the function that will be called to handle the request
type Handler func(r *http.Request) (interface{}, error)
