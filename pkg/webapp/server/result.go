package server

// Result contains a result object
type Result struct {
	Result string `json:"result"`
}

// NewResult returns a new result object
func NewResult(result string) Result {
	return Result{Result: result}
}
