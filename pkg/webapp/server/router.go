package server

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/google/martian/cors"
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

// Middleware is the alias for middleware handlers
type Middleware func(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc)

// NewRouter creates a new router with the routes specified.
func NewRouter(root string, routes []Route, middlewares []Middleware, isCORS bool) http.Handler {
	router := mux.NewRouter()
	registerRoutes(router, root, routes)
	r := registerMiddleware(router, middlewares)
	return enableCORS(r, isCORS)
}

func registerRoutes(router *mux.Router, root string, routes []Route) {
	fmt.Println("APIs registered: ")
	for _, route := range routes {
		handler := renderResponse(route.HandlerFunc, route.Status)
		router.
			Methods(route.Method).
			Path(fmt.Sprintf("%s%s", root, route.Pattern)).
			Name(route.Name).
			Handler(handler)

		fmt.Printf("> %s %s%s: %s\n", route.Method, root, route.Pattern, route.Name)
	}
	fmt.Println()
}

type errorResponse struct {
	Msg string `json:"error"`
}

func renderResponse(handler Handler, statusCode int) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		res, err := handler(req)
		w.Header().Set("Content-Type", "application/json")
		code := statusCode

		var body []byte

		if err == nil {
			body, err = json.Marshal(res)
		}
		if err != nil {
			body, _ = json.Marshal(errorResponse{Msg: err.Error()})

			errObj, ok := err.(Err)
			if ok {
				code = errObj.Code
			} else {
				code = http.StatusInternalServerError
			}
		}

		w.WriteHeader(code)
		fmt.Fprint(w, string(body))
	}
}

func registerMiddleware(router *mux.Router, middlewares []Middleware) http.Handler {
	n := negroni.Classic()
	for _, mw := range middlewares {
		n.UseFunc(mw)
	}
	n.UseHandler(router)
	return n
}

func enableCORS(handler http.Handler, isCORS bool) http.Handler {
	if isCORS {
		fmt.Println("cors activated.")
		return cors.NewHandler(handler)
	}
	return handler
}
