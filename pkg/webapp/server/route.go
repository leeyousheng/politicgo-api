package server

import "net/http"

// Route struct contains the neccessary information for defining a route.
type Route struct {
	Name        string
	Method      string
	Pattern     string
	Status      int
	HandlerFunc Handler
}

// NewGetRoute creates a new route for use in the controller with GET as method
func NewGetRoute(name, pattern string, handler Handler) Route {
	return newRoute(name, "GET", pattern, handler, http.StatusOK)
}

// NewPostRoute creates a new route for use in the controller with POST as method
func NewPostRoute(name, pattern string, handler Handler) Route {
	return newRoute(name, "POST", pattern, handler, http.StatusCreated)
}

// NewPutRoute creates a new route for use in the controller with PUT as method
func NewPutRoute(name, pattern string, handler Handler) Route {
	return newRoute(name, "PUT", pattern, handler, http.StatusOK)
}

// NewDeleteRoute creates a new route for use in the controller with DELETE as method
func NewDeleteRoute(name, pattern string, handler Handler) Route {
	return newRoute(name, "DELETE", pattern, handler, http.StatusOK)
}

// NewPatchRoute creates a new route for use in the controller with PATCH as method
func NewPatchRoute(name, pattern string, handler Handler) Route {
	return newRoute(name, "PATCH", pattern, handler, http.StatusOK)
}

func newRoute(name, method, pattern string, handler Handler, status int) Route {
	return Route{Name: name, Method: method, Pattern: pattern, HandlerFunc: handler, Status: status}
}
