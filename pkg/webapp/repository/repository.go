package repository

// Repository interface is contains the methods that can be used to communicate with the db.
type Repository interface {
	Find() ([][]byte, error)
	FindOne(id string) ([]byte, error)
	Create(id string, entity interface{}, name string) ([]byte, error)
	CreateWithGenID(entity interface{}, name string) ([]byte, error)
	Update(id string, entity interface{}, name string) ([]byte, error)
	Delete(id string) error
}
