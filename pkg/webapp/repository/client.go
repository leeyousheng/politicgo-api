package repository

// Client represents the common interface for db connection.
type Client interface {
	Connect() error
	Disconnect() error
}
