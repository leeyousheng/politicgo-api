package firebase

import (
	"context"
	"encoding/json"
	"time"

	waEntity "bitbucket.org/leeyousheng/politicgo-api/pkg/webapp/entity"
	"cloud.google.com/go/firestore"
)

// Repository handles the data of the user.
type Repository struct {
	Db *firestore.CollectionRef
}

// NewFirebaseRepository instantiate a new repository with the collection ref.
func NewFirebaseRepository(db *firestore.CollectionRef) Repository {
	return Repository{Db: db}
}

// FindPaged retrieves the resoruces according to pagination style
func (r *Repository) FindPaged(res interface{}, pageConfig PageConfig, criterias []CriteriaConfig) error {
	query := r.Db.Query
	if pageConfig != nil {
		if o := pageConfig.OrderBy(); o != nil {
			fsDir, err := convDirection(o.Direction())
			if err != nil {
				return err
			}
			query = query.OrderBy(o.Key(), fsDir)
		}

		if pageConfig.Cursor() != "" {
			if t, err := time.Parse("2006-01-02T15:04:05.000000Z", pageConfig.Cursor()); err == nil {
				query = query.StartAfter(t)
			} else {
				query = query.StartAfter(pageConfig.Cursor())
			}
		}

		query = query.Limit(pageConfig.Size())
	}

	for _, v := range criterias {
		query = query.Where(v.Field, v.Operator, v.Value)
	}

	snapshots, err := query.Documents(context.Background()).GetAll()
	if err != nil {
		return err
	}

	col := make([]interface{}, len(snapshots))
	for i, s := range snapshots {
		s.DataTo(&col[i])
	}

	return toObject(col, res)
}

// Query returns the firestore Query object related to the DB.
func (r *Repository) Query() firestore.Query {
	return r.Db.Query
}

// Find retrieves the resoruces
func (r *Repository) Find(res interface{}) error {
	return r.FindPaged(res, nil, []CriteriaConfig{})
}

// FindOne returns a reference
func (r *Repository) FindOne(id string, res interface{}) error {
	s, err := r.Db.Doc(id).Get(context.Background())
	if err != nil {
		return err
	}

	d := map[string]interface{}{}
	s.DataTo(&d)
	return toObject(d, res)
}

// Create persists a new entity with the given ID
func (r *Repository) Create(id string, entity interface{}, name string) error {
	var err error

	if e, ok := entity.(waEntity.Distinct); ok {
		performCreateOverwrites(e, id, name)
	}

	_, err = r.Db.Doc(id).Set(context.Background(), entity)
	return err
}

// CreateWithGenID persists a new entity using a generated ID
func (r *Repository) CreateWithGenID(entity interface{}, name string) error {
	return r.Create(r.Db.NewDoc().ID, entity, name)
}

// FullUpdate the already persisted data with new values fully
func (r *Repository) FullUpdate(id string, res interface{}, name string) error {
	if err := performUpdateOverwrites(r, res, id, name); err != nil {
		return err
	}

	_, err := r.Db.Doc(id).Set(context.Background(), res)
	return err
}

// Update the already persisted data with new existing inpu values
func (r *Repository) Update(id string, input map[string]interface{}, name string, res interface{}) error {
	if err := r.FindOne(id, res); err != nil {
		return err
	}

	entity := map[string]interface{}{}
	if err := toObject(res, &entity); err != nil {
		return err
	}

	for k, v := range input {
		entity[k] = v
	}
	if err := toObject(entity, &res); err != nil {
		return err
	}

	if err := performUpdateOverwrites(r, res, id, name); err != nil {
		return err
	}

	_, err := r.Db.Doc(id).Set(context.Background(), res)
	return err
}

// Delete the entity
func (r *Repository) Delete(id string) error {
	_, err := r.Db.Doc(id).Delete(context.Background())
	return err
}

func toObject(data interface{}, result interface{}) error {
	j, err := json.Marshal(data)
	if err != nil {
		return err
	}

	err = json.Unmarshal(j, result)
	return err
}

func performUpdateOverwrites(r *Repository, entity interface{}, id string, name string) error {
	if e, ok := entity.(waEntity.Auditable); ok {
		ref := waEntity.AuditableStruct{}
		err := r.FindOne(id, &ref)
		if err != nil {
			return err
		}

		e.OverwriteAuditFields(&ref)
		e.PopulateUpdatedInfo(name)
	}

	if e, ok := entity.(waEntity.Distinct); ok {
		e.OverwriteDistinctFields(id)
	}
	return nil
}

func performCreateOverwrites(entity interface{}, id string, name string) {
	if e, ok := entity.(waEntity.Auditable); ok {
		e.PopulateCreatedInfo(name)
	}

	if e, ok := entity.(waEntity.Distinct); ok {
		e.OverwriteDistinctFields(id)
	}
}
