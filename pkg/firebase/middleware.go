package firebase

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	firebase "firebase.google.com/go"
)

// NewAuth returns the initialised firebase auth middleware
func NewAuth(firebase *firebase.App) func(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	authClient, err := firebase.Auth(context.Background())
	if err != nil {
		panic(err)
	}

	return func(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
		authorization := strings.TrimSpace(r.Header.Get("Authorization"))
		if authorization != "" {
			token, err := authClient.VerifyIDToken(context.Background(), strings.Replace(authorization, "Bearer ", "", 1))
			if err != nil {
				fmt.Fprint(rw, err.Error())
				return
			}
			r.Header.Set("uid", token.UID)
		}
		next(rw, r)
	}
}
