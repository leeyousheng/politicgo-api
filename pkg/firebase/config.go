package firebase

import (
	"fmt"
	"strings"

	"cloud.google.com/go/firestore"
)

// PageConfig contains the fields needed for pagination
type PageConfig interface {
	OrderBy() OrderBy
	Size() int
	Cursor() string
}

// CriteriaConfig contains the fields for criteria
type CriteriaConfig struct {
	Field    string
	Operator string
	Value    interface{}
}

// OrderBy contains the fields needed for sorting
type OrderBy interface {
	Key() string
	Direction() string
}

func convDirection(direction string) (firestore.Direction, error) {
	switch strings.ToLower(direction) {
	case "desc":
		return firestore.Desc, nil
	case "asc":
		return firestore.Asc, nil
	default:
		return firestore.Asc, fmt.Errorf("Invalid direction %s", direction)
	}
}
