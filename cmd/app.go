package cmd

import (
	"log"

	"bitbucket.org/leeyousheng/politicgo-api/internal/cmd"
)

// Run starts the application.
func Run() {
	c := cmd.InitApp()
	defer c.Terminate()
	log.Fatal(c.Serve())
}
