# Politicgo API #
This repository contains the source code for the politicgo backend.

## Scripts ##
Tested on go1.14.2 runtime.
### build.sh ###
- `./scripts/build.sh install` installs the application
- `./scripts/build.sh test` perform all tests in the project
- `./scripts/build.sh tidy` tidy up project modules
- `./scripts/build.sh lint` perform linting on project
### localserver.sh ###
- `./scripts/localserver.sh run` installs, tests and run a local instance on port `8080`
### deploy.sh ###
- `./scripts/deploy.sh app` deploys the app to GCP
- `./scripts/deploy.sh endpoints` deploys the swagger to GCP endpoints
- `./scripts/deploy.sh firestore` deploys the firestore rules and index.