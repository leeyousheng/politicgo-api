#!/bin/bash
for cmd in "$@"
do
  case $cmd in
    app)
      echo "* Deploying App *"
      gcloud app deploy --appyaml="./deployments/gcp/app.yaml" --version=0-2-0
    ;;
    endpoints)
      echo "* Deploying Endpoints *"
      gcloud endpoints services deploy ./api/swagger.yaml --project eus-politicgo-webapp
    ;;
    firestore)
      echo "* Deploying Firestore Configurations"
      cd ./deployments/gcp
      firebase deploy --only firestore
      cd ../..
    ;;
    
    all)
      sh ./scripts/deploy.sh app endpoints firestore
    ;;
  esac
done
