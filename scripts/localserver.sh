#!/bin/bash
for cmd in "$@"
do
  case $cmd in
    run)
      echo "* performing run *"
      ./scripts/build.sh install test
      
      export GOOGLE_APPLICATION_CREDENTIALS="./test/firebase/credentials.local.json"
      $GOPATH/bin/politicgo-api
  esac
done